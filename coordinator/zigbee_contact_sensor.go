package coordinator

import (
	"adai.design/homemaster/container/accessory"
	"adai.design/homemaster/container/characteristic"
	"encoding/binary"
	"adai.design/homemaster/log"
)

// 小米接触式传感器

type zigbeeContactSensor struct {
	*ZigbeeDescriptor
	acc *accessory.ContactSensor
}

func (z *zigbeeContactSensor) update(data []byte) error {
	srcAddr := binary.BigEndian.Uint16(data[1:])
	endpoint := data[3]
	value := data[11]
	log.Printf("attribute: netaddr(%04x) endpoint(%d) data(%d)\n", srcAddr, endpoint, value)

	if endpoint == 0x01 {
		if value == 0x00 {
			z.acc.Contact.ContactSensorState.UpdateValue(characteristic.ContactSensorStateContactNotDetected)
		} else {
			z.acc.Contact.ContactSensorState.UpdateValue(characteristic.ContactSensorStateContactDetected)
		}
	}
	return nil
}

func newContactSensor(zigbee *ZigbeeDescriptor) *zigbeeContactSensor {
	sensor := &zigbeeContactSensor{
		ZigbeeDescriptor: zigbee,
		acc: accessory.NewContactSensor(zigbee.MacAddr),
	}
	return sensor
}


const xiaoMiContactSensorEndpointDefault =
	`[
		{
             "endpoint": 1,
             "profile_id": "0104",
             "device_id": "0104",
             "input_cluster": [
                 "0000",
                 "0003",
                 "0019",
                 "ffff"
             ],
             "output_cluster": [
                 "0000",
                 "0003",
                 "0019",
                 "ffff",
                 "d5b7",
                 "0402",
                 "0403"
             ],
             "manufacturer": "LUMI",
             "mode": "lumi.sensor_magnet",
             "date": "20160516"
        }
     ]`