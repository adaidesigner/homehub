package coordinator

import (
	"testing"
	"os"
	"adai.design/homemaster/log"
	"time"
	"io/ioutil"
	"encoding/base64"
)

func getConfigFilePath() string {
	filepath := os.Getenv("GOPATH") + "/src/adai.design/homemaster/build/tmp/"
	file := filepath + "coordinator.cfg"
	return file
}

var testAccessories = []*ZigbeeDescriptor{
	{
		NetAddr: "6B47",
		MacAddr: "00158d000288fb3d",
		DevType: DevTypeRouter,
	},
	{
		NetAddr: "F5EC",
		MacAddr: "00158d000215112f",
		DevType: DevTypeRouter,
	},
	{
		NetAddr: "89D8",
		MacAddr: "00158d000215112f",
		DevType: DevTypeRouter,
	},
	{
		NetAddr: "F5EC",
		MacAddr: "d0cf5efffe11f992",
		DevType: DevTypeEndDevice,
	},
	{
		NetAddr: "E9D9",
		MacAddr: "001788010116b471",
		DevType: DevTypeRouter,
	},
	{
		NetAddr: "E500",
		MacAddr: "00178801024490f7",
		DevType: DevTypeRouter,
	},
	{
		NetAddr: "07F6",
		MacAddr: "0017880103a0f87e",
		DevType: DevTypeRouter,
	},
	{
		NetAddr: "5157",
		MacAddr: "00158d0002379233",
		DevType: DevTypeEndDevice,
	},
}

func TestZigbeeSave(t *testing.T)  {
	file := getConfigFilePath()
	log.Info("file: ", file)

	manager.load()

	for _, v := range testAccessories {
		manager.Add(v.NetAddr, v.MacAddr, v.DevType)
	}
}

func TestZigbeeDeviceAnnounce(t *testing.T) {
	StartCoordinator()

	for {
		time.Sleep(time.Second)
		if len(manager.accessories) > 0 {
			break
		}
	}

	for {
		time.Sleep(time.Second * 1)
		if manager.zigbeeEndpointCheck() {
			break
		}
	}

	data, err := ioutil.ReadFile(configFileDefault)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("accessory: %s", string(data))
}

func TestEndPointEncode(t *testing.T) {
	data, err  := base64.StdEncoding.DecodeString("AQIDZA==")
	if err != nil {
		t.Fatal(err)
	}
	t.Log(data)
}





















