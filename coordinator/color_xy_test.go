package coordinator

import (
	"testing"
)


// moveToLevel
// 01 02 10 81 02 10 02 19 4E 02 12 E9 D9 02 11 02 1B 02 10 FF 02 10 02 11 03
// 01 00 81 00 09 4E 02 E9 D9 01 0B 00 FF 00 01 03
//01 0081 0009 4E 02 E9D9 01 0B 00 FF 0001 03

// moveToColor
// 01 02 10 B7 02 10 02 1B 02 13 02 12 E9 D9 02 11 02 1B 28 31 02 14 9B 02 10 02 11 03
// 01 00 B7 00 0B 03 02 E9 D9 01 0B 28 31 04 9B 00 01 03
//01 00B7 000B 03 02 E9D9 01 0B 2831 049B 0001 03




func TestColorXY(t *testing.T) {
	x, y := rgbToCIEXY(255, 0, 255)
	t.Logf("x: %04x y: %04x\n", x, y)

}