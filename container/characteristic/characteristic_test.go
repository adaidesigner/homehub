package characteristic

import (
	"testing"
	"encoding/json"
	"net"
	"time"
)

var TestConn *websocket.Conn = &fakeConn{}

type fakeConn struct {
}

func (f *fakeConn) Read(b []byte) (n int, err error) {
	return 0, nil
}

func (f *fakeConn) Write(b []byte) (n int, err error) {
	return 0, nil
}

func (f *fakeConn) Close() error {
	return nil
}

func (f *fakeConn) LocalAddr() net.Addr {
	return nil
}

func (f *fakeConn) RemoteAddr() net.Addr {
	return nil
}

func (f *fakeConn) SetDeadline(t time.Time) error {
	return nil
}

func (f *fakeConn) SetReadDeadline(t time.Time) error {
	return nil
}

func (f *fakeConn) SetWriteDeadline(t time.Time) error {
	return nil
}

func TestCharacteristicEncode(t *testing.T) {
	t.Log("let's go!")
	c := NewCharacteristic(TypeOn)
	data, _ := json.Marshal(c)
	t.Log("json: ", string(data))
}

func TestCharacteristicLocalDelegate(t *testing.T) {
	c := NewCharacteristic(TypeOn)
	c.Value = 5

	var oldValue interface{}
	var newValue interface{}

	c.OnValueUpdate(func(c *Characteristic, new, old interface{}) {
		oldValue = old
		newValue = new
	})

	c.UpdateValue(10)
	c.UpdateValueFromConnection(20, TestConn)

	if is, want := oldValue, 5; is != want {
		t.Fatalf("is=%v want=%v", is, want)
	}

	if is, want := newValue, 10; is != want {
		t.Fatalf("is=%v want=%v", is, want)
	}
}

func TestCharacteristicRemoteDelegate(t *testing.T) {
	c := NewCharacteristic(TypeOn)
	c.Value = 5

	var oldValue interface{}
	var newValue interface{}

	c.OnValueUpdateFromConn(func(conn *websocket.Conn, c *Characteristic, new, old interface{}) {
		if conn != TestConn {
			t.Fatal(conn)
		}
		newValue = new
		oldValue = old
	})

	c.UpdateValueFromConnection(10, TestConn)
	c.UpdateValue(20)

	if is, want := oldValue, 5; is != want {
		t.Fatalf("is=%v, want=%v", is, want)
	}

	if is, want := newValue, 10; is != want {
		t.Fatalf("is=%v, want=%v", is, want)
	}
}
