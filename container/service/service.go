package service

import "adai.design/homemaster/container/characteristic"

type Service struct {
	ID              int                              `json:"sid"`
	Type            string                           `json:"type"`
	State           string                           `json:"state,omitempty"`
	Characteristics []*characteristic.Characteristic `json:"characteristics"`
}

func (s *Service) SetId(id int)  {
	s.ID = id
}

func (s *Service) GetId() int {
	return s.ID
}

func New(typ string) *Service {
	s := Service{
		Type:            typ,
		Characteristics: []*characteristic.Characteristic{},
	}
	return &s
}

func (s *Service) AddCharacteristic(c *characteristic.Characteristic) {
	s.Characteristics = append(s.Characteristics, c)
}

// 服务名字
const NameSevId = 0

func (s *Service) SetName(name string) {

	var c *characteristic.Characteristic
	for _, v := range s.Characteristics {
		if v.Type == characteristic.TypeName {
			c = v
		}
	}

	if c != nil {
		c.Value = name
		return
	}

	cName := characteristic.NewName()
	cName.Id = NameSevId
	cName.SetValue(name)
	s.AddCharacteristic(cName.Characteristic)
	return
}
