package client

import (
	"encoding/json"
)

type Message struct {
	Path	string				`json:"path"`
	Method 	string				`json:"method"`
	State 	string				`json:"state,omitempty"`
	Data	json.RawMessage		`json:"data,omitempty"`
}

const MessageSlice = '\n'

const (
	MsgPathLogin = "login"
	MsgPathHeartbeat = "hb"
	MsgPathAccessories = "container"
	MsgPathCharacteristic = "characteristics"
	MsgPathContainer = "container/info"
	MsgPathBind = "bind"
)

const (
	MsgMethodPost = "post"
	MsgMethodGet = "get"
	MsgMethodPut = "put"
)

